package com.mcpayment.technicaltest.controller.mapper;

import com.mcpayment.technicaltest.datatransferobject.MerchantDTO;
import com.mcpayment.technicaltest.domainobject.MerchantDO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class MerchantMapperTest {

    @Test
    void makeMerchantDOTest() {
        MerchantDTO merchantDTO = MerchantDTO.builder()
                .setUsername("merchant01")
                .setPassword("merchant01pw")
                .build();
        MerchantDO actual = MerchantMapper.makeMerchantDO(merchantDTO);
        Assertions.assertEquals("merchant01", actual.getUsername());
        Assertions.assertEquals("merchant01pw", actual.getPassword());
    }

    @Test
    void makeMerchantDTOTest() {
        MerchantDO merchantDO = new MerchantDO("merchant01", "merchant01pw");
        MerchantDTO actual = MerchantMapper.makeMerchantDTO(merchantDO);
        Assertions.assertEquals("merchant01", actual.getUsername());
        Assertions.assertEquals("merchant01pw", actual.getPassword());
    }

    @Test
    void makeMerchantDTOListTest() {
        List<MerchantDO> merchantDOList = Arrays.asList(
                new MerchantDO("merchant01", "merchant01pw"),
                new MerchantDO("merchant02", "merchant02pw"),
                new MerchantDO("merchant03", "merchant03pw"));
        List<MerchantDTO> actual = MerchantMapper.makeMerchantDTOList(merchantDOList);
        Assertions.assertEquals(3, actual.size());
    }

}
