package com.mcpayment.technicaltest.service.merchant;

import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainvalue.MerchantType;
import com.mcpayment.technicaltest.exception.ConstraintViolationException;
import com.mcpayment.technicaltest.exception.EntityNotFoundException;

import java.util.List;

public interface MerchantService {

    MerchantDO find(Long merchantId) throws EntityNotFoundException;

    MerchantDO create(MerchantDO merchantDO) throws ConstraintViolationException;

    void delete(Long merchantId) throws EntityNotFoundException;

    void updateMerchantType(long merchantId, MerchantType merchantType) throws EntityNotFoundException;

    List<MerchantDO> find(MerchantType merchantType);
}
