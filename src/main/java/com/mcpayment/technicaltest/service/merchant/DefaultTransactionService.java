package com.mcpayment.technicaltest.service.merchant;

import com.mcpayment.technicaltest.dataaccessobject.PaymentRepository;
import com.mcpayment.technicaltest.dataaccessobject.TransactionRepository;
import com.mcpayment.technicaltest.domainobject.PaymentDO;
import com.mcpayment.technicaltest.domainobject.TransactionDO;
import com.mcpayment.technicaltest.domainvalue.PaymentChannel;
import com.mcpayment.technicaltest.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class DefaultTransactionService implements TransactionService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTransactionService.class);

    private final  TransactionRepository transactionRepository;
    private final PaymentRepository paymentRepository;

    public DefaultTransactionService(TransactionRepository transactionRepository, PaymentRepository paymentRepository) {
        this.transactionRepository = transactionRepository;
        this.paymentRepository = paymentRepository;
    }

    @Override
    public TransactionDO find(Long id) throws EntityNotFoundException {
        return findTransactionChecked(id);
    }

    @Override
    public TransactionDO create(TransactionDO transactionDO) throws ConstraintViolationException {
        TransactionDO transaction;
        try {
            transaction = transactionRepository.save(transactionDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("ConstraintsViolationException while creating a transaction: {}", transactionDO, e);
            throw new ConstraintViolationException(e.getMessage());
        }
        return transaction;
    }

    @Override
    @Transactional
    public void delete(Long transactionId) throws EntityNotFoundException {
        TransactionDO transaction = findTransactionChecked(transactionId);
        transactionRepository.delete(transaction);
    }

    @Override
    public List<TransactionDO> find(PaymentChannel paymentChannel) {
        return transactionRepository.findByPaymentChannel(paymentChannel);
    }

    @Override
    public List<TransactionDO> findMerchantId(long id) throws MerchantNotFoundException {
        List<TransactionDO> list =  transactionRepository.findByMerchantDO_Id(id);
        if(list.isEmpty()) {
            throw new MerchantNotFoundException("could not find merchant with id : " + id);
        }
        return list;
    }

    @Override
    @Transactional
    public PaymentDO processTransaction(long transactionId, String issuerName, long amount) throws EntityNotFoundException, TransactionAlreadyPaidException, InvalidAmountException {
        TransactionDO transaction = find(transactionId);
        if(!transaction.isPaid()) {
            if(amount != transaction.getAmount()) {
                throw new InvalidAmountException();
            }
            transaction.setPaid(true);
            transaction.setPaidTime(ZonedDateTime.now());
            PaymentDO paymentDO = new PaymentDO(issuerName, amount, transaction);
            paymentDO.setPaymentStatus("succes");
            PaymentDO savePayment =  paymentRepository.save(paymentDO);
            System.out.println(paymentDO.getTransactionDO().isPaid());
            return savePayment;
        }
        throw new TransactionAlreadyPaidException();
    }


    private TransactionDO findTransactionChecked(long id) throws EntityNotFoundException {
        return transactionRepository.findById(id).
                orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + id));
    }
}
