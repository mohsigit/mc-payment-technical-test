package com.mcpayment.technicaltest.service.merchant;


import com.mcpayment.technicaltest.domainobject.PaymentDO;
import com.mcpayment.technicaltest.domainobject.TransactionDO;
import com.mcpayment.technicaltest.domainvalue.PaymentChannel;
import com.mcpayment.technicaltest.exception.*;

import java.util.List;

public interface TransactionService {

    TransactionDO find(Long id) throws EntityNotFoundException;

    TransactionDO create(TransactionDO transactionDO) throws ConstraintViolationException;

    void delete(Long transactionId) throws EntityNotFoundException;

    List<TransactionDO> find(PaymentChannel paymentChannel);

    List<TransactionDO> findMerchantId(long id) throws MerchantNotFoundException;

    PaymentDO processTransaction(long transactionId, String issuerName, long amount) throws EntityNotFoundException, InvalidAmountException, TransactionAlreadyPaidException;
}
