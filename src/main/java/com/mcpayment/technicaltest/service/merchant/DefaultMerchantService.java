package com.mcpayment.technicaltest.service.merchant;

import com.mcpayment.technicaltest.dataaccessobject.MerchantRepository;
import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainvalue.MerchantType;
import com.mcpayment.technicaltest.exception.ConstraintViolationException;
import com.mcpayment.technicaltest.exception.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DefaultMerchantService implements MerchantService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultMerchantService.class);

    private final MerchantRepository merchantRepository;

    public DefaultMerchantService(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    /**
     * Selects a merchant by id.
     *
     * @param merchantId
     * @return found merchant
     * @throws EntityNotFoundException if no merchant with the given id was found.
     */
    @Override
    public MerchantDO find(Long merchantId) throws EntityNotFoundException {
        return findMerchantChecked(merchantId);
    }

    /**
     * Creates a new merchant.
     *
     * @param merchantDO
     * @return
     * @throws ConstraintViolationException if a merchant already exists with the given username, ... .
     */
    @Override
    public MerchantDO create(MerchantDO merchantDO) throws ConstraintViolationException {
        MerchantDO merchant;
        try {
            merchant = merchantRepository.save(merchantDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("ConstraintsViolationException while creating a merchant: {}", merchantDO, e);
            throw new ConstraintViolationException(e.getMessage());
        }
        return merchant;
    }

    /**
     * Deletes an existing merchant by id.
     *
     * @param merchantId
     * @throws EntityNotFoundException if no merchant with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long merchantId) throws EntityNotFoundException {
        MerchantDO merchantDO = findMerchantChecked(merchantId);
        merchantDO.setDeleted(true);
    }

    /**
     * Update the merchant type for a merchant.
     *
     * @param merchantId
     * @param merchantType
     * @throws EntityNotFoundException
     */
    @Override
    @Transactional
    public void updateMerchantType(long merchantId, MerchantType merchantType) throws EntityNotFoundException {
        MerchantDO merchantDO = findMerchantChecked(merchantId);
        merchantDO.setMerchantType(merchantType);
    }

    /**
     * Find all merchants by merchant type.
     *
     * @param merchantType
     */
    @Override
    public List<MerchantDO> find(MerchantType merchantType) {
        return merchantRepository.findByMerchantTypeAndDeleted(merchantType, false);
    }

    private MerchantDO findMerchantChecked(Long merchantId) throws EntityNotFoundException {
        return merchantRepository.findById(merchantId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + merchantId));
    }

}
