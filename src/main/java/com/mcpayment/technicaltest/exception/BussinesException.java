package com.mcpayment.technicaltest.exception;

public abstract  class BussinesException extends Exception {
    public BussinesException(String message) {
        super(message);
    }
}
