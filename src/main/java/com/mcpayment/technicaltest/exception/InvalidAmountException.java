package com.mcpayment.technicaltest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


public class InvalidAmountException extends BussinesException {
    public InvalidAmountException(String message)
    {
        super(message);
    }

    public InvalidAmountException()
    {
        super("invalid amount");
    }
}
