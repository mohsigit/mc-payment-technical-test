package com.mcpayment.technicaltest.exception;




public class TransactionAlreadyPaidException extends BussinesException {
    public TransactionAlreadyPaidException()
    {
        super("transaction is already paid");
    }
    public TransactionAlreadyPaidException(String message)
    {
        super(message);
    }
}
