package com.mcpayment.technicaltest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Merchant is not found.")
public class MerchantNotFoundException extends Exception {
    static final long serialVersionUID = -8987516993334229948L;
    public MerchantNotFoundException(String message)
    {
        super(message);
    }
}
