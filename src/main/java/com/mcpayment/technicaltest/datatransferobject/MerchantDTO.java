package com.mcpayment.technicaltest.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mcpayment.technicaltest.domainvalue.MerchantType;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MerchantDTO {

    private Long id;

    @NotNull(message = "Username can not be null!")
    private String username;

    @NotNull(message = "Password can not be null!")
    private String password;

    private MerchantType merchantType;

    private MerchantDTO() {
    }

    public MerchantDTO(Long id, String username, String password, MerchantType merchantType) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.merchantType = merchantType;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static MerchantDTOBuilder builder() {
        return new MerchantDTOBuilder();
    }

    public static class MerchantDTOBuilder {
        private Long id;
        private String username;
        private String password;
        private MerchantType merchantType;

        public MerchantDTOBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public MerchantDTOBuilder setUsername(String username) {
            this.username = username;
            return this;
        }

        public MerchantDTOBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public MerchantDTOBuilder setMerchantType(MerchantType merchantType) {
            this.merchantType = merchantType;
            return this;
        }

        public MerchantDTO build() {
            return new MerchantDTO(id, username, password, merchantType);
        }

    }
}
