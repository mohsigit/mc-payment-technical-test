package com.mcpayment.technicaltest.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mcpayment.technicaltest.domainvalue.PaymentChannel;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentDTO {

    long paymentId;

    @NotNull
    long transactionId;

    @NotNull
    @Size(min = 1, max = 40)
    private String issuerName;

    @NotNull
    @Min(1)
    private long amount;

    private ZonedDateTime paymentDate= ZonedDateTime.now();

    private PaymentChannel paymentChannel;
    private String transactionStatus;

    public PaymentDTO(long paymentId, long transactionId, String issuerName, long amount, ZonedDateTime paymentDate, PaymentChannel paymentChannel, String transactionStatus) {
        this.paymentId = paymentId;
        this.transactionId = transactionId;
        this.issuerName = issuerName;
        this.amount = amount;
        this.paymentDate = paymentDate;
        this.paymentChannel = paymentChannel;
        this.transactionStatus = transactionStatus;
    }

    @JsonProperty
    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public ZonedDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(ZonedDateTime paymentDateb) {
        this.paymentDate = paymentDateb;
    }


    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public PaymentChannel getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(PaymentChannel paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public static PaymentDTOBuilder builder() {
        return new PaymentDTOBuilder();
    }

    public static class PaymentDTOBuilder {

        long paymentId;
        long transactionId;
        private String issuerName;
        private long amount;
        private ZonedDateTime paymentDate;
        private PaymentChannel paymentChannel;
        private String transactionStatus;

        public PaymentDTOBuilder setPaymentId(long paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public PaymentDTOBuilder setTransactionId(long transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public PaymentDTOBuilder setIssuerName(String issuerName) {
            this.issuerName = issuerName;
            return this;
        }


        public PaymentDTOBuilder setAmount(long amount) {
            this.amount = amount;
            return this;
        }

        public PaymentDTOBuilder setPaymentDate(ZonedDateTime paymentDate) {
            this.paymentDate = paymentDate;
            return this;
        }

        public PaymentDTOBuilder setTransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
            return this;
        }

        public PaymentDTOBuilder setPaymentChannel(PaymentChannel paymentChannel) {
            this.paymentChannel = paymentChannel;
            return this;
        }


        public PaymentDTO build() {
            return new PaymentDTO(paymentId, transactionId, issuerName, amount, paymentDate, paymentChannel, transactionStatus);
        }




    }
}
