package com.mcpayment.technicaltest.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mcpayment.technicaltest.dataprojection.TopMerchantView;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopMerchantViewDTO {
    long id;
    String merchantName;
    long totalTransaksi;


    private TopMerchantViewDTO(long id, String merchantName, long totalTransaksi) {
        this.id = id;
        this.merchantName = merchantName;
        this.totalTransaksi = totalTransaksi;
    }

    public static TopMerchantViewDTO newInstance(TopMerchantView view) {
        return new TopMerchantViewDTO(view.getId(), view.getUsername(), view.getTotal());
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public long getTotalTransaksi() {
        return totalTransaksi;
    }

    public void setTotalTransaksi(long totalTransaksi) {
        this.totalTransaksi = totalTransaksi;
    }
}
