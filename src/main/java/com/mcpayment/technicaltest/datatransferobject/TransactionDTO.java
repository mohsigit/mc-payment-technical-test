package com.mcpayment.technicaltest.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mcpayment.technicaltest.domainvalue.PaymentChannel;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionDTO {

    private long id;

    @NotNull
    private PaymentChannel paymentChannel;

    @NotNull
    @Min(1)
    private long amount;


    private ZonedDateTime expiredTime;

    private ZonedDateTime paidTime;

    private ZonedDateTime dateCreated = ZonedDateTime.now();

    private long merchantId;

    private String merchantName;



    public TransactionDTO() {

    }

    public TransactionDTO(long id, PaymentChannel paymentChannel,long amount,
                          ZonedDateTime expiredTime, ZonedDateTime paidTime, long merchantId, String merchantName) {
        this.id = id;
        this.paymentChannel = paymentChannel;
        this.amount = amount;
        this.expiredTime = expiredTime;
        this.paidTime = paidTime;
        this.merchantId = merchantId;
        this.merchantName = merchantName;
    }



    @JsonProperty
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PaymentChannel getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(PaymentChannel paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public ZonedDateTime getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(ZonedDateTime expiredTime) {
        this.expiredTime = expiredTime;
    }

    public ZonedDateTime getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(ZonedDateTime paidTime) {
        this.paidTime = paidTime;
    }

    public long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(long merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }


    public static TransactionDTOBuilder builder() {
        return new TransactionDTOBuilder();
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public static class TransactionDTOBuilder {
        private long id;
        private PaymentChannel paymentChannel;
        private long amount;
        private ZonedDateTime expiredTime;
        private ZonedDateTime paidTime;
        private long merchantId;
        private String merchantName;
        private boolean paid;


        public TransactionDTOBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public TransactionDTOBuilder setPaymentChannel(PaymentChannel paymentChannel) {
            this.paymentChannel = paymentChannel;
            return this;
        }

        public TransactionDTOBuilder setAmount(long amount) {
            this.amount = amount;
            return this;
        }

        public TransactionDTOBuilder setExpiredTime(ZonedDateTime expiredTime) {
            this.expiredTime = expiredTime;
            return this;
        }

        public TransactionDTOBuilder setPaidTime(ZonedDateTime paidTime) {
            this.paidTime = paidTime;
            return this;
        }

        public TransactionDTOBuilder setMerchantId(long id) {
            this.merchantId = id;
            return this;
        }

        public TransactionDTOBuilder setMerchantName(String name) {
            this.merchantName = name;
            return this;
        }

        public TransactionDTOBuilder setPaid(boolean paid) {
            this.paid = paid;
            return this;
        }

        public TransactionDTO build() {
            return new TransactionDTO(id, paymentChannel, amount, expiredTime, paidTime, merchantId, merchantName);
        }






    }
}
