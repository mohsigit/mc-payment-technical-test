package com.mcpayment.technicaltest.dataaccessobject;

import com.mcpayment.technicaltest.domainobject.TransactionDO;
import com.mcpayment.technicaltest.domainvalue.PaymentChannel;
import com.mcpayment.technicaltest.dataprojection.TopMerchantView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository  extends CrudRepository<TransactionDO, Long> {
    List<TransactionDO> findByPaymentChannel(PaymentChannel channel);

    List<TransactionDO> findByMerchantDO_Id(long merchantId);

    @Query(nativeQuery = true, value =
            "SELECT " +
                    "    SUM(v.amount) AS total, m.username, m.id " +
                    "FROM " +
                    "    Transaction v JOIN Merchant m ON v.merchant_id = m.id " +
                    "GROUP BY " +
                    "    m.id" + " ORDER BY total DESC")
    List<TopMerchantView> getTopMerchants();

}
