package com.mcpayment.technicaltest.dataaccessobject;

import com.mcpayment.technicaltest.domainobject.PaymentDO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<PaymentDO, Long> {

    List<PaymentDO>findByTransactionDO_Paid(boolean paid, Pageable pageable);
}
