package com.mcpayment.technicaltest.controller;


import com.mcpayment.technicaltest.controller.mapper.PaymentMapper;
import com.mcpayment.technicaltest.controller.mapper.TransactionMapper;
import com.mcpayment.technicaltest.datatransferobject.PaymentDTO;
import com.mcpayment.technicaltest.datatransferobject.TransactionDTO;
import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainobject.TransactionDO;
import com.mcpayment.technicaltest.exception.*;
import com.mcpayment.technicaltest.service.merchant.MerchantService;
import com.mcpayment.technicaltest.service.merchant.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/transactions")
public class TransactionController {

    private final TransactionService transactionService;

    private final MerchantService merchantService;

    private static final Logger LOG = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    public TransactionController(TransactionService transactionService, MerchantService merchantService) {
        this.transactionService = transactionService;
        this.merchantService = merchantService;
    }

    @GetMapping("/{transactionId}")
    public TransactionDTO getTransaction(@PathVariable long transactionId) throws EntityNotFoundException {
        return TransactionMapper.makeTransactionDTO(transactionService.find(transactionId));
    }

    @DeleteMapping("/{transactionId}")
    public void deleteTransaction(@PathVariable long transactionId)  {
        try {
            transactionService.delete(transactionId);
        } catch (EntityNotFoundException e) {
            LOG.debug("Transaksi dengan id " + transactionId + " tidak ditemukan");
        }
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TransactionDTO createTransaction(@Valid @RequestBody TransactionDTO transactionDTO) throws ConstraintViolationException, EntityNotFoundException {
        MerchantDO merchant = merchantService.find(transactionDTO.getMerchantId());
        TransactionDO transactionDO = TransactionMapper.makeTransactionDO(transactionDTO, merchant);
        return TransactionMapper.makeTransactionDTO(transactionService.create(transactionDO));
    }

    @PostMapping("pay")
    @ResponseStatus(HttpStatus.CREATED)
    public PaymentDTO createPayment(@Valid @RequestBody PaymentDTO paymentDTO) throws ConstraintViolationException, EntityNotFoundException, InvalidAmountException, TransactionAlreadyPaidException {
        return PaymentMapper.makePaymentDTO(transactionService.processTransaction(paymentDTO.getTransactionId(), paymentDTO.getIssuerName(), paymentDTO.getAmount()));
    }


    @GetMapping()
    public List<TransactionDTO> findTransactionsMerchant(@RequestParam long merchantId) throws MerchantNotFoundException {
        return TransactionMapper.makeTransactionDTOList(transactionService.findMerchantId(merchantId));
    }


}
