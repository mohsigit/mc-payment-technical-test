package com.mcpayment.technicaltest.controller;

import com.mcpayment.technicaltest.controller.mapper.MerchantMapper;
import com.mcpayment.technicaltest.controller.mapper.PaymentMapper;
import com.mcpayment.technicaltest.dataaccessobject.PaymentRepository;
import com.mcpayment.technicaltest.dataaccessobject.TransactionRepository;
import com.mcpayment.technicaltest.datatransferobject.PaymentDTO;
import com.mcpayment.technicaltest.datatransferobject.TopMerchantViewDTO;
import com.mcpayment.technicaltest.domainobject.PaymentDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/reports")
public class ReportController {

    private final PaymentRepository paymentRepository;

    private final TransactionRepository transactionRepository;

    @Autowired
    public ReportController(PaymentRepository paymentRepository, TransactionRepository transactionRepository) {
        this.paymentRepository = paymentRepository;
        this.transactionRepository = transactionRepository;
    }

    @GetMapping("/transactions")
    public List<PaymentDTO> getSuccesTransaction(@RequestParam(defaultValue = "10") int limit) {
        System.out.println("Limit : " + limit);
        List<PaymentDO> list =  paymentRepository.findByTransactionDO_Paid(true, PageRequest.of(0, limit));
        System.out.println("Size : " + list.size());
        return PaymentMapper.makePaymentDTOList(list);
    }

    @GetMapping("/topMerchants")
    public List<TopMerchantViewDTO> getTopMerchants() {
        return MerchantMapper.makeTopMerchantViewDTOList(transactionRepository.getTopMerchants());
    }



}
