package com.mcpayment.technicaltest.controller.handler;

import com.mcpayment.technicaltest.exception.BussinesException;
import com.mcpayment.technicaltest.exception.InvalidAmountException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(value = BussinesException.class)
    public ResponseEntity invalidAmountException(BussinesException exception) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ApiException message = new ApiException(exception.getMessage(), status.value(), ZonedDateTime.now());
        return new ResponseEntity<ApiException>(message, status);

    }



    private static class ApiException {
        private String message;
        private int httpStatus;
        private ZonedDateTime timestamp;

        public ApiException(String message, int httpStatus, ZonedDateTime timestamp) {
            this.message = message;
            this.httpStatus = httpStatus;
            this.timestamp = timestamp;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getHttpStatus() {
            return httpStatus;
        }

        public void setHttpStatus(int httpStatus) {
            this.httpStatus = httpStatus;
        }

        public ZonedDateTime getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(ZonedDateTime timestamp) {
            this.timestamp = timestamp;
        }
    }

}
