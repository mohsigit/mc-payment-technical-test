package com.mcpayment.technicaltest.controller.mapper;

import com.mcpayment.technicaltest.datatransferobject.TransactionDTO;
import com.mcpayment.technicaltest.domainobject.MerchantDO;
import com.mcpayment.technicaltest.domainobject.TransactionDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionMapper {

    public static TransactionDO makeTransactionDO(TransactionDTO dto, MerchantDO merchantDO) {
        TransactionDO transactionDO = new TransactionDO(dto.getAmount(), dto.getPaymentChannel(), dto.getPaidTime(), dto.getExpiredTime(), merchantDO);
        return transactionDO;
    }

    public static TransactionDTO makeTransactionDTO(TransactionDO transactionDO) {
        return TransactionDTO.builder()
                .setId(transactionDO.getId())
                .setAmount(transactionDO.getAmount())
                .setExpiredTime(transactionDO.getExpiredTime())
                .setPaidTime(transactionDO.getPaidTime())
                .setPaymentChannel(transactionDO.getPaymentChannel())
                .setMerchantId(transactionDO.getMerchantDO().getId())
                .setMerchantName(transactionDO.getMerchantDO().getUsername())
                .build();
    }



    public static List<TransactionDTO> makeTransactionDTOList(Collection<TransactionDO> transactions) {
        return transactions.stream()
                .map(TransactionMapper::makeTransactionDTO)
                .collect(Collectors.toList());
    }
}
