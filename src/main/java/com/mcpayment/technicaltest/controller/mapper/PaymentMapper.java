package com.mcpayment.technicaltest.controller.mapper;


import com.mcpayment.technicaltest.datatransferobject.PaymentDTO;
import com.mcpayment.technicaltest.domainobject.PaymentDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class PaymentMapper {

    public static PaymentDTO makePaymentDTO(PaymentDO paymentDO) {
        return PaymentDTO.builder()
                .setPaymentId(paymentDO.getId())
                .setIssuerName(paymentDO.getIssuerName())
                .setAmount(paymentDO.getAmount())
                .setTransactionId(paymentDO.getTransactionDO().getId())
                .setPaymentDate(paymentDO.getPaymentDate())
                .setTransactionStatus(paymentDO.getPaymentStatus())
                .setPaymentChannel(paymentDO.getTransactionDO().getPaymentChannel()).build();

    }

    public static List<PaymentDTO> makePaymentDTOList(Collection<PaymentDO> payments) {
        return payments.stream()
                .map(PaymentMapper::makePaymentDTO)
                .collect(Collectors.toList());
    }
}
