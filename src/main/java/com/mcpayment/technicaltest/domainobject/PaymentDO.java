package com.mcpayment.technicaltest.domainobject;


import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Entity
@Table(
        name = "payment"
)
public class PaymentDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false)
    @Size(min = 3, max = 30)
    private String issuerName;

    @Column(nullable = false)
    private long amount;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime paymentDate = ZonedDateTime.now();

    @ManyToOne
    private TransactionDO transactionDO;

    private String paymentStatus;


    public PaymentDO() {

    }

    public PaymentDO(String issuerName, long amount, TransactionDO transactionDO) {
        this.issuerName = issuerName;
        this.amount = amount;
        this.transactionDO = transactionDO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public ZonedDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(ZonedDateTime paymentDateb) {
        this.paymentDate = paymentDateb;
    }

    public TransactionDO getTransactionDO() {
        return transactionDO;
    }

    public void setTransactionDO(TransactionDO transactionDO) {
        this.transactionDO = transactionDO;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }


}
