package com.mcpayment.technicaltest.domainobject;

import com.mcpayment.technicaltest.domainvalue.PaymentChannel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(
        name = "transaction"
)
public class TransactionDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long amount;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private PaymentChannel paymentChannel;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime paidTime;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime expiredTime = ZonedDateTime.now();

    @ManyToOne
    @JoinColumn(name = "merchant_id")
    private MerchantDO merchantDO;

    private boolean paid;


    public TransactionDO() {
        paid = false;
    }

    public TransactionDO(Long amount, PaymentChannel paymentChannel,ZonedDateTime paidTime, ZonedDateTime expiredTime, MerchantDO merchantDO) {
        this.amount = amount;
        this.paymentChannel = paymentChannel;
        this.dateCreated = dateCreated;
        this.paidTime = paidTime;
        this.expiredTime = expiredTime;
        this.merchantDO = merchantDO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public PaymentChannel getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(PaymentChannel paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public ZonedDateTime getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(ZonedDateTime paidTime) {
        this.paidTime = paidTime;
    }

    public ZonedDateTime getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(ZonedDateTime expiredTime) {
        this.expiredTime = expiredTime;
    }

    public MerchantDO getMerchantDO() {
        return merchantDO;
    }

    public void setMerchantDO(MerchantDO merchantDO) {
        this.merchantDO = merchantDO;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public String toString() {
        return "TransactionDO{" +
                "id=" + id +
                ", amount=" + amount +
                ", paymentChannel=" + paymentChannel +
                ", dateCreated=" + dateCreated +
                ", paidTime=" + paidTime +
                ", expiredTime=" + expiredTime +
                ", merchantDO id=" + merchantDO.getId() +
                ", merchantDO username=" + merchantDO.getUsername() +
                '}';
    }
}
