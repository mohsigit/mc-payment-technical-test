package com.mcpayment.technicaltest.dataprojection;

public interface TopMerchantView {

    Long getId();
    long getTotal();
    String getUsername();


}
