package com.mcpayment.technicaltest.domainvalue;

public enum MerchantType
{
    PREMIUM, REGULAR
}