package com.mcpayment.technicaltest.domainvalue;

public enum PaymentChannel {

    DANA, OVO, SHOPEEPAY, LINKAJA

}
